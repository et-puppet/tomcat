# Class: tomcat
# ===========
#
# Class to configure a Tomcat base image
#
# Examples
# --------
#
# @example
#
#    include 'tomcat'
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2018 The Board of Trustees of the Leland Stanford Junior
# University
#
class tomcat (
  $user,
  $uid,
  $group,
  $gid,
  $home,
  $conf_dir,
  $tomcat_pkg,
  $tcnative_pkg,
  $java_home,
  $java_pkg,
  $servlet_pkg,
) {

  user { $user:
    ensure         => present,
    uid            => $uid,
    gid            => $gid,
    home           => $home,
    managehome     => false,
    system         => true,
    shell          => '/bin/false',
    purge_ssh_keys => true,
    require        => Group[$group],
  }

  group { $group:
    ensure => present,
    gid    => $gid,
    system => true,
  }

  include 'apt'

  Exec['apt_update'] -> Package<| |>

  Package[$java_pkg] -> Package[$tomcat_pkg]
  Package[$java_pkg] -> Package[$servlet_pkg]

  package { [
    $java_pkg,
    $tomcat_pkg,
    $tcnative_pkg,
    $servlet_pkg,
  ]:
    ensure  => latest,
    require => [
      Group[$group],
      User[$user]
    ],
  }

  file { [
    '/app',
    '/app/conf',
    '/app/webapp',
    ]:
    ensure  => directory,
    owner   => 'root',
    group   => $group,
    mode    => '0755',
    require => Group[$group],
  }

  #
  # Tomcat Configuration
  #

  file { "${conf_dir}/server.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/tomcat/server.xml",
    require => Package[$tomcat_pkg],
  }

  file { "${conf_dir}/web.xml":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/tomcat/web.xml",
    require => Package[$tomcat_pkg],
  }

  file { "/tmp/${user}":
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => '0755',
    require => User[$user],
  }

}
